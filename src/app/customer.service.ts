import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';
import { Currency, CustomerRaw } from './interfaces/customer-raw';
import { IncomeData } from './interfaces/income-data';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  customerCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');
  END_POINT_URL = 'https://c9r3vc1eh1.execute-api.us-east-1.amazonaws.com/beta';


  constructor(private db:AngularFirestore,private http:HttpClient) { }

  addCustomer(userId:string,customerData:CustomerRaw){
    const customerRef = this.userCollection.doc(userId).collection('customers').doc();
    customerRef.set({
      id:customerRef.ref.id,
      ...customerData
    })
  }
  
  getCustomersById(userId:string){
    this.customerCollection = this.userCollection.doc(userId).collection('customers');
    return this.customerCollection.snapshotChanges();
  }

  delete(userId:string,customerId:string){
    this.customerCollection = this.userCollection.doc(userId).collection('customers');
    this.customerCollection.doc(customerId).delete();
    this.customerCollection.snapshotChanges();
  }

  predict(userId:string,customerId:string,incomeData:IncomeData){
  this.customerCollection = this.userCollection.doc(userId).collection('customers');
  const predictData = {data:`${incomeData.totalRelationship},${incomeData.inactiveMonth},${incomeData.numberOfContacts},${incomeData.totalRevolvingBal},${incomeData.totalCtChangeQ1ToQ4},${incomeData.totalTransChange},${incomeData.totalTransCount},${incomeData.totalCtChangeQ1ToQ4},${incomeData.avgRatio}`};
  this.http.post(this.END_POINT_URL,predictData).subscribe(res => {
   this.customerCollection.doc(customerId).update({
      predict:!!res && res === 'yes'
   })
  return this.customerCollection.snapshotChanges();
  })
  }
  getAllCustomers(userId:string){
    this.customerCollection = this.userCollection.doc(userId).collection('customers');
    return this.customerCollection.snapshotChanges();
  }


  saveConvert(userId:string,customerId:string,currency:Currency,newSalary:number,selectedCustomer:any){
    this.customerCollection = this.userCollection.doc(userId).collection('customers');

    this.customerCollection.doc(customerId).update({
      generalData:{
        ...selectedCustomer.generalData,
        currency,
        salary:newSalary
      }
    })
    return this.customerCollection.snapshotChanges();
  }

editCustomer(userId:string,customerId:string,editData:any,selectedCustomer){
  this.customerCollection = this.userCollection.doc(userId).collection('customers');
  this.customerCollection.doc(customerId).update({
    generalData:{
      ...selectedCustomer.generalData,
      salary:editData.generalData.salary
    },
    incomeData:{
      ...selectedCustomer.IncomeData,
      ...editData.incomeData
    },
    predict:-1
  })
  this.customerCollection.snapshotChanges();
  localStorage.removeItem(customerId);

}
};

