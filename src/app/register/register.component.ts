import { User } from './../interfaces/user';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import {FormControl, Validators} from '@angular/forms';


interface Role{
  value:number;
  viewValue:string;
}

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})


export class RegisterComponent implements OnInit {

  email:string;
  name:string;
  password:string;
  confirmedPassword:string;
  emailErrorMessage:string;
  passwordsError:string;
  // roles:string[] = ['Bank Manager','Department Manager','Accounts Manager'];
  roles: Role[] = [
    {value: 2, viewValue: 'Bank Manager'},
    {value: 1, viewValue:'Accounts Manager' }
  ];
    onSubmit(){
    if(this.password === this.confirmedPassword){
      const registeredUser = {
        email:this.email,
        displayName:this.name,
      }
      this.auth.register(this.email,this.password)
       .then(res => {
         console.log({res});
         this.auth.addUser(res.user.uid,registeredUser);
        this.router.navigate(['/login']);
      })
      .catch(err =>{
        this.emailErrorMessage = err.message;
      })
    }else{
     this.passwordsError = 'the passwords did not match';
    }
  }

  clearError(){
    this.emailErrorMessage = '';
    this.passwordsError = '';
  }

  constructor(private auth:AuthService,private router:Router) { }

  ngOnInit(): void {
  }

}
