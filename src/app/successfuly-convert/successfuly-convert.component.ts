import { Component } from '@angular/core';

@Component({
  selector: 'successfuly-convert',
  templateUrl: './successfuly-convert.component.html',
  styleUrls: ['./successfuly-convert.component.css'],
  styles: [`
    .success {
      color: #2591c6;
      text-align:center;
    }
  `]
})
export class SuccessfulyConvertComponent {}

